drop database pweb_2022_kelas_2;
create database pweb_2022_kelas_2;
use pweb_2022_kelas_2;

create table dosen(
    nidn varchar(10) not null primary key,
    nama varchar(100) not null,
    tanggal_lahir date not null,
    alamat text not null,
    jenis_kelamin enum('L','P') default 'L'
);

insert into dosen values
('234519','Adi','1999-01-01','Jogja','L'),
('234529','Ida','1999-02-02','Jogja','P'),
('234539','Edi','1999-03-03','Jogja','L'),
('234549','Susi','1999-04-04','Jogja','P'),
('234559','Joko','1999-05-05','Jogja','L');

insert into dosen values
('234569','Bowo','1999-05-05','Jogja','L');

create table mobil(
  id int not null primary key auto_increment,
  plat_no varchar(10),
  merek varchar(100),
  tipe varchar(100),
  dosen_nidn varchar(10) not null,
  foreign key(dosen_nidn) references dosen(nidn),
  unique(plat_no)
);

insert into mobil values
(null, "AB 3453 HJ","Honda","Brio","234539"),
(null, "AB 2352 AK","Toyota","Avanza","234539");
insert into mobil values
(null, "AB 2462 AK","Toyota","Avanza","234559");

alter table mobil add
gambar varchar(100) default null;
