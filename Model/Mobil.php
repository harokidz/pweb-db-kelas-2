<?php
include_once __DIR__ . '/../Config/Koneksi.php';
include_once __DIR__ . '/Dosen.php';

class Mobil
{
    public $id;
    public $platNo;
    public $merek;
    public $tipe;
    public $dosenNidn;
    public $gambar;
    public $dosen;

    public static function getAll(): array
    {
        $query = "select * from mobil";
        $conn = new Koneksi();
        $mysqlResult =  mysqli_query($conn->koneksi, $query);
        $result = [];
        while ($mblDB = mysqli_fetch_object($mysqlResult)) {
            $result[] = self::setFields($mblDB);
        }
        return $result;
    }
    public static function getByNidn($nidn): array
    {
        $query = "select * from mobil where dosen_nidn='$nidn'";
        $conn = new Koneksi();
        $mysqlResult =  mysqli_query($conn->koneksi, $query);
        $result = [];
        while ($mblDB = mysqli_fetch_object($mysqlResult)) {
            $result[] = self::setFieldsWithoutDosen($mblDB);
        }
        return $result;
    }

    public static function getByPrimaryKey($id): ?Mobil
    {

        $query = "select * from mobil where id='$id'";
        $conn = new Koneksi();
        $mysqlResult =  mysqli_query($conn->koneksi, $query);
        $result = null;
        while ($mblDB = mysqli_fetch_object($mysqlResult)) {
            $result = self::setFields($mblDB);
        }

        return $result;
    }

    private static function setFields($mblDB): Mobil
    {
        $mobilObj = new Mobil();
        $mobilObj->id = $mblDB->id;
        $mobilObj->platNo = $mblDB->plat_no;
        $mobilObj->merek = $mblDB->merek;
        $mobilObj->tipe = $mblDB->tipe;
        $mobilObj->gambar = $mblDB->gambar;
        $mobilObj->dosenNidn = $mblDB->dosen_nidn;
        $mobilObj->dosen = Dosen::getByPrimaryKey($mblDB->dosen_nidn);
        return $mobilObj;
    }
    private static function setFieldsWithoutDosen($mblDB): Mobil
    {
        $mobilObj = new Mobil();
        $mobilObj->id = $mblDB->id;
        $mobilObj->platNo = $mblDB->plat_no;
        $mobilObj->merek = $mblDB->merek;
        $mobilObj->tipe = $mblDB->tipe;
        $mobilObj->gambar = $mblDB->gambar;
        $mobilObj->dosenNidn = $mblDB->dosen_nidn;
        return $mobilObj;
    }

    public function insert(): void
    {
        $query = "insert into mobil(plat_no,merek,tipe,gambar,dosen_nidn) values "
            . "("
            . "'$this->platNo',"
            . "'$this->merek',"
            . "'$this->tipe',"
            . "'$this->gambar',"
            . "'$this->dosenNidn'"
            . ")";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }
}
