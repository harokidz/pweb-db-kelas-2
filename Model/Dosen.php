<?php

include_once __DIR__ . '/../Config/Koneksi.php';
include_once __DIR__ . '/Mobil.php';

class Dosen
{
    public $nidn;
    public $nama;
    public $tanggalLahir;
    public $alamat;
    public $jenisKelamin;
    public $jkLengkap;
    public $cars;

    /**
     * Ini adalah fungsi untuk mendapatkan semua data dosen
     * @return array Data berupa array dari object Dosen
     */
    public static function getAll(): array
    {
        $query = "select * from dosen";
        $conn = new Koneksi();
        $mysqlResult =  mysqli_query($conn->koneksi, $query);
        $result = [];
        while ($dosenDB = mysqli_fetch_object($mysqlResult)) {
            $dosen = new Dosen();
            $dosen->nidn = $dosenDB->nidn;
            $dosen->nama = $dosenDB->nama;
            $dosen->tanggalLahir = $dosenDB->tanggal_lahir;
            $dosen->alamat = $dosenDB->alamat;
            $dosen->jenisKelamin = $dosenDB->jenis_kelamin;
            $dosen->jkLengkap = $dosen->jenisKelamin == 'L' ? 'Laki-laki' : "Perempuan";
            $dosen->cars = Mobil::getByNidn($dosenDB->nidn);
            $result[] = $dosen;
        }
        return $result;
    }

    public function insert(): void
    {
        $query = "insert into dosen(nidn,nama,tanggal_lahir,alamat, jenis_kelamin) values ("
            . "'$this->nidn',"
            . "'$this->nama',"
            . "'$this->tanggalLahir',"
            . "'$this->alamat',"
            . "'$this->jenisKelamin'"
            . ")";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }

    public static function insertByObject(Dosen  $dosen): void
    {
        $query = "insert into dosen(nidn,nama,tanggal_lahir,alamat, jenis_kelamin) values ("
            . "'$dosen->nidn',"
            . "'$dosen->nama',"
            . "'$dosen->tanggalLahir',"
            . "'$dosen->alamat',"
            . "'$dosen->jenisKelamin'"
            . ")";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }

    public static function insertByParameter($nidn, $nama, $tanggalLahir, $alamat, $jk): void
    {
        $query = "insert into dosen(nidn,nama,tanggal_lahir,alamat, jenis_kelamin) values ("
            . "'$nidn',"
            . "'$nama',"
            . "'$tanggalLahir',"
            . "'$alamat',"
            . "'$jk'"
            . ")";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }

    /**
     * Function untuk mendapatkan data berdasarkan primary key
     * 
     */
    public static function getByPrimaryKey($primaryKey): ?Dosen
    {
        $query = "select * from dosen where nidn='$primaryKey'";
        $conn = new Koneksi();
        $mysqlResult =  mysqli_query($conn->koneksi, $query);
        $result = null;
        while ($dosenDB = mysqli_fetch_object($mysqlResult)) {
            $dosen = new Dosen();
            $dosen->nidn = $dosenDB->nidn;
            $dosen->nama = $dosenDB->nama;
            $dosen->tanggalLahir = $dosenDB->tanggal_lahir;
            $dosen->alamat = $dosenDB->alamat;
            $dosen->jenisKelamin = $dosenDB->jenis_kelamin;
            $dosen->jkLengkap = $dosen->jenisKelamin == 'L' ? 'Laki-laki' : "Perempuan";
            $dosen->cars = Mobil::getByNidn($dosenDB->nidn);
            $result = $dosen;
        }
        return $result;
    }


    public function update(): void
    {
        $query = "update dosen set "
            . "nama='$this->nama',"
            . "tanggal_lahir='$this->tanggalLahir',"
            . "alamat='$this->alamat',"
            . "jenis_kelamin='$this->jenisKelamin' "
            . "where nidn='$this->nidn'";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }
    
    public function delete(): void
    {
        $query = "delete from dosen where nidn='$this->nidn'";
        $conn = new Koneksi();
        mysqli_query($conn->koneksi, $query);
    }
}
