<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.css" />
    <script src="https://code.jquery.com/jquery-3.6.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.13.1/datatables.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/additional-methods.min.js"></script>
    <title>Document</title>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>Pemrograman Web</h2>
                <nav class="navbar navbar-expand-lg  navbar-dark bg-dark">
                    <a class="navbar-brand" href="#">Apps</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="index.php">Home</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=list-dosen">List Dosen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=add-dosen">Tambah Dosen</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=list-mobil">List Mobil</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="index.php?page=add-mobil">Tambah Mobil</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div class="col-12 mt-2">
                <?php
                $halaman = "view/home.php";
                if (isset($_REQUEST['page'])) {
                    $page = $_REQUEST['page'];
                    switch ($page) {
                        case "list-dosen":
                            $halaman = "view/dosen/index.php";
                            break;
                        case "add-dosen":
                            $halaman = "view/dosen/addForm.php";
                            break;
                        case "update-dosen":
                            $halaman = "view/dosen/updateForm.php";
                            break;
                        case "list-mobil":
                            $halaman = "view/mobil/index.php";
                            break;
                        case "add-mobil":
                            $halaman = "view/mobil/addForm.php";
                            break;
                    }
                }
                include $halaman;
                ?>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            
            $('[data-toggle="tooltip"]').tooltip();
        })
    </script>
</body>

</html>