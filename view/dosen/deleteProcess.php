<?php
include_once __DIR__ . "/../../Model/Dosen.php";
$nidn = $_REQUEST['nidn'];
$dosen = Dosen::getByPrimaryKey($nidn);
if ($dosen === NULL) {
    echo "<h2>Data Dosen tidak ditemukan</h2>";
    echo "<a href='index.php'>Kembali</a>";
    die();
} else {
    #proses hapus
    $dosen->delete();
    #redirect ke halaman index
    header('Location: /../../index.php?page=list-dosen');
}
