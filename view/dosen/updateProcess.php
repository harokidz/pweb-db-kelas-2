<?php
include_once __DIR__ . "/../../Model/Dosen.php";
$nidn = $_REQUEST['nidn'];
$dosen = Dosen::getByPrimaryKey($nidn);
if ($dosen === NULL) {
    echo "<h2>Data Dosen tidak ditemukan</h2>";
    echo "<a href='index.php'>Kembali</a>";
    die();
} else {
    #proses ubah
    $nama = $_REQUEST['nama'];
    $alamat = $_REQUEST['alamat'];
    $tanggalLahir = $_REQUEST['tanggalLahir'];
    $jenisKelamin = $_REQUEST['jenisKelamin'];

    #3. Set data ke dalam field dosen
    $dosen->nama = $nama;
    $dosen->alamat = $alamat;
    $dosen->tanggalLahir = $tanggalLahir;
    $dosen->jenisKelamin = $jenisKelamin;
    
    $dosen->update();
    
    #redirect ke halaman index
    header('Location: index.php');
}
