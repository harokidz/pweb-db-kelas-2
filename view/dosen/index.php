<?php
include_once __DIR__ . '/../../Model/Dosen.php';
?>

<div class="card">
    <div class="card-header">
        <h4>Data Dosen</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="table-dosen" class="table table-striped">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>NIDN</th>
                        <th>Nama</th>
                        <th>Tanggal Lahir</th>
                        <th>Jenis Kelamin</th>
                        <th>Alamat</th>
                        <th>Mobil</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $listDosen = Dosen::getAll();
                    $nomer = 1;
                    foreach ($listDosen as $dosen) {
                    ?>
                        <tr>
                            <td><?= $nomer++ ?></td>
                            <td><?= $dosen->nidn ?></td>
                            <td><?= $dosen->nama ?></td>
                            <td><?= $dosen->tanggalLahir ?></td>
                            <td><?= $dosen->jenisKelamin ?></td>
                            <td><?= $dosen->alamat ?></td>
                            <td>
                                Punya <?= count($dosen->cars) ?> Mobil : <br>
                                <?php
                                foreach ($dosen->cars as $mobil) {
                                    echo "$mobil->merek $mobil->tipe ($mobil->platNo) <br>";
                                }
                                ?>
                            </td>
                            <td>
                                <a class="btn btn-warning btn-sm" data-toggle="tooltip" data-placement="top" title="Ubah Data Dosen" href="index.php?page=update-dosen&nidn=<?= $dosen->nidn ?>">
                                    <i class="bi bi-pencil-square"></i>
                                </a>
                                <a data-nidn='<?= $dosen->nidn ?>' class="btn btn-danger btn-sm btn-hapus" href="#">
                                    <i class="bi bi-trash"></i>
                                </a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#table-dosen').DataTable();
        $(".btn-hapus").click(function() {
            var nidn = $(this).data('nidn');
            $.confirm({
                title: 'Konfirmasi!',
                content: 'Anda yakin hapus data dosen dengan NIDN ' + nidn + ' ?',
                buttons: {
                    cancel: function() {},
                    hapus: {
                        text: 'Hapus',
                        btnClass: 'btn-red',
                        action: function() {
                            $.LoadingOverlay("show");
                            window.location.href = 'view/dosen/deleteProcess.php?nidn='+nidn;
                        }
                    }
                }
            });
        });
    });
</script>