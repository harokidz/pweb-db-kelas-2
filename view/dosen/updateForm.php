<?php
include_once __DIR__ . "/../../Model/Dosen.php";
$nidn = $_REQUEST['nidn'];
$dosen = Dosen::getByPrimaryKey($nidn);
if ($dosen === NULL) {
    echo "<h2>Data Dosen tidak ditemukan</h2>";
    echo "<a href='index.php'>Kembali</a>";
    die();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>PWEB | Tambah Dosen Baru</title>
</head>

<body>
    <h2>Tambah Data Dosen</h2>
    <form action="updateProcess.php" method="POST">
        <p>NIDN :
            <input required readonly value="<?= $dosen->nidn ?>" type="text" name="nidn">
        </p>
        <p>Nama : <input required value="<?= $dosen->nama ?>" type="text" name="nama"></p>
        <p>Tanggal Lahir :
            <input required value="<?= $dosen->tanggalLahir ?>" type="date" name="tanggalLahir">
        </p>
        <p>Alamat : 
            <input required value="<?= $dosen->alamat ?>" type="text" name="alamat">
        </p>
        <p>Jenis Kelamin :
            <input required <?= $dosen->jenisKelamin == 'L' ? 'checked' : '' ?> 
            type="radio" name="jenisKelamin" value="L" />Laki-laki

            <input required <?= $dosen->jenisKelamin == 'P' ? 'checked' : '' ?> 
            type="radio" name="jenisKelamin" value="P" />Perempuan
        </p>
        <a href="index.php">Kembali</a>
        <button type="reset">Reset</button>
        <button type="submit">Simpan Data</button>
    </form>
</body>

</html>