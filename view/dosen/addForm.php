<div class="card">
    <div class="card-header">
        <h4>Tambah Data Dosen</h4>
    </div>
    <div class="card-body">
        <form id="form-dosen" action="view/dosen/addProcess.php" method="POST">
            <div class="form-group col-4">
                <label for="">NIDN</label>
                <input required class="form-control" type="text" name="nidn">
            </div>
            <div class="form-group col-4">
                <label for="">Nama</label>
                <input required class="form-control" type="text" name="nama">
            </div>
            <div class="form-group col-4">
                <label for="">Tanggal Lahir</label>
                <input required class="form-control" type="date" name="tanggalLahir">
            </div>
            <div class="form-group col-4">
                <label for="">Alamat</label>
                <input required class="form-control" type="text" name="alamat">
            </div>
            <div class="form-group col-4">
                <label for="">Jenis Kelamin</label> <br>
                <input required type="radio" name="jenisKelamin" value="L" />Laki-laki
                <input required type="radio" name="jenisKelamin" value="P" />Perempuan
            </div>

            <button class="btn btn-success" type="submit">
                <i class="bi bi-save2"></i>
                Simpan Data
            </button>
        </form>
    </div>
</div>

<script>
    $(function() {
        jQuery.validator.setDefaults({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        $('#form-dosen').validate();
    });
</script>