<?php
include_once __DIR__. "/../../Model/Dosen.php";
$nidn = $_REQUEST['nidn'];
$dosen = Dosen::getByPrimaryKey($nidn);
if($dosen === NULL){
   echo "<h2>Data Dosen tidak ditemukan</h2>";
   echo "<a href='index.php'>Kembali</a>";
   die();
}
?>
<!DOCTYPE html>
<html lang="en">
    <title>Konfirmasi Hapus</title>
</head>
<body>
    <h2>Anda yakin hapus data ini?</h2>
    <p>NIDN : <?= $dosen->nidn ?></p>
    <p>Nama : <?= $dosen->nama ?></p>
    <p>Tangal Lahir : <?= $dosen->tanggalLahir ?></p>
    <p>Jenis Kelamin : <?= $dosen->jenisKelamin ?></p>
    <p>Alamat : <?= $dosen->alamat ?></p>
    <a href="index.php">Batal</a>
    <a href="deleteProcess.php?nidn=<?=$dosen->nidn ?>">Hapus</a>
</body>
</html>