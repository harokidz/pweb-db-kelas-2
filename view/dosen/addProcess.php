<?php
include_once __DIR__ . '/../../Model/Dosen.php';

#1. Ambil semua data yang di kirim
$nidn = $_REQUEST['nidn'];
$nama = $_REQUEST['nama'];
$alamat = $_REQUEST['alamat'];
$tanggalLahir = $_REQUEST['tanggalLahir'];
$jenisKelamin = $_REQUEST['jenisKelamin'];

#2. Buat Objek Baru dari Dosen
$dosen = new Dosen();

#3. Set data ke dalam field dosen
$dosen->nidn = $nidn;
$dosen->nama = $nama;
$dosen->alamat = $alamat;
$dosen->tanggalLahir = $tanggalLahir;
$dosen->jenisKelamin = $jenisKelamin;

#4 Panggil function insert
$dosen->insert();
// Dosen::insertByObject($dosen);
// Dosen::insertByParameter($nidn,$nama,$tanggalLahir,$alamat,$jenisKelamin);

#5 Redirect kembali ke halaman index
header('Location: /../../index.php?page=list-dosen');
