<?php
include_once __DIR__ . '/../../Model/Mobil.php';
$listMobil = Mobil::getAll();
?>
<div class="card">
    <div class="card-header">
        <h4>Data Mobil Dosen</h4>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Gambar</th>
                        <th>Plat Nomer</th>
                        <th>Merek</th>
                        <th>Tipe</th>
                        <th>Pemilik</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no = 1;
                    foreach ($listMobil as $mobil) {
                    ?>
                        <tr>
                            <td><?= $no++ ?></td>
                            <td><img class="img-thumbnail" style="max-width:100px !important;" src="/images/<?= $mobil->gambar ?>" alt="" onerror="this.onerror=null;this.src='/images/no-image.jpg';">
                            </td>
                            <td><?= $mobil->platNo ?></td>
                            <td><?= $mobil->merek ?></td>
                            <td><?= $mobil->tipe ?></td>
                            <td>
                                Nidn : <?= $mobil->dosen->nidn ?> <br>
                                Nama : <?= $mobil->dosen->nama ?>
                            </td>
                            <td>
                                <a class="btn btn-warning btn-sm" href="updateForm.php?id=<?= $mobil->id ?>">
                                    <i class="bi bi-pencil-square"></i>
                                </a>
                            </td>
                        </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>