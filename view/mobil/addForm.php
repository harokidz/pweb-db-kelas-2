<?php
include_once __DIR__ . '/../../Model/Dosen.php';
$listDosen = Dosen::getAll();
?>
<div class="card">
    <div class="card-header">
        <h4>Tambah Data Mobil</h4>
    </div>
    <div class="card-body">
        <form id="form-mobil" action="view/mobil/addProcess.php" method="POST" enctype="multipart/form-data">
            <div class="form-group col-4">
                <label for="">Dosen Pemilik Mobil</label>
                <select required class="form-control" name="dosenNidn" id="select-dosen">
                    <option value="" disabled selected>Pilih Dosen</option>
                    <?php
                    foreach ($listDosen as $dosen) {
                        echo "<option value='$dosen->nidn' >$dosen->nidn | $dosen->nama</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-4">
                <label for="">Plat No</label>
                <input required class="form-control" type="text" name="platNo" id="">
            </div>
            <div class="form-group col-4">
                <label for="">Merek</label>
                <input required class="form-control" type="text" name="merek" id="">
            </div>
            <div class="form-group col-4">
                <label for="">Tipe</label>
                <input required class="form-control" type="text" name="tipe" id="">
            </div>
            <div class="form-group col-4">
                <label for="">Gambar</label>
                <input required class="form-control" type="file" name="gambar" />
            </div>

            <button type="submit" class="btn btn-success" type="submit">Simpan</button>
        </form>
        </body>

        </html>
    </div>
</div>
<script>
    $(function() {
        $("#select-dosen").select2();
        jQuery.validator.setDefaults({
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
        $('#form-mobil').validate();
    });
</script>