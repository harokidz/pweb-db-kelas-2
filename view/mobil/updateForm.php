<?php
include_once __DIR__ . '/../../Model/Mobil.php';
include_once __DIR__ . '/../../Model/Dosen.php';
$id = $_REQUEST['id'];
$mobil = Mobil::getByPrimaryKey($id);
if ($mobil === NULL) {
    echo "<h2>Data Mobil tidak ditemukan</h2>";
    echo "<a href='index.php'>Kembali</a>";
    die();
}
#Kondisi jika mobil nya ada
$listDosen = Dosen::getAll();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambha Data Mobil</title>
</head>

<body>
    <h3>Tambah Data Mobil</h3>
    <form action="addProcess.php" method="POST">
        <p>
            Dosen Pemilik Mobil : <br>
            <select name="dosenNidn" id="">
                <option value="" disabled selected>Pilih Dosen</option>
                <?php
                foreach ($listDosen as $dosen) {
                    $selected = "";
                    if ($dosen->nidn === $mobil->dosenNidn) {
                        $selected = "selected";
                    }
                    echo "<option $selected value='$dosen->nidn' >$dosen->nidn | $dosen->nama</option>";
                }
                ?>
            </select>
        </p>
        <p>Plat No : <br><input value="<?= $mobil->platNo ?>" type="text" name="platNo" id=""></p>
        <p>Merek : <br><input value="<?= $mobil->merek ?>" type="text" name="merek" id=""></p>
        <p>Tipe : <br><input value="<?= $mobil->tipe ?>"  type="text" name="tipe" id=""></p>
        <button type="submit">Simpan</button>
    </form>
</body>

</html>