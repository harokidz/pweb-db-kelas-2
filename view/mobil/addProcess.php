<?php
include_once __DIR__.'/../../Model/Mobil.php';
include_once __DIR__.'/../../Model/Upload.php';
#1.Ambil semua parameter
$platNo = $_REQUEST['platNo'];
$merek = $_REQUEST['merek'];
$tipe = $_REQUEST['tipe'];
$dosenNidn = $_REQUEST['dosenNidn'];

#2. Buat Objek dari Mobil
$mobil = new Mobil();

#3. Set semua parameter
$mobil->platNo = $platNo;
$mobil->merek = $merek;
$mobil->tipe = $tipe;
$mobil->dosenNidn = $dosenNidn;

#3.1 Proses Upload Gambar
$namaFile = Upload::image();
if($namaFile == false){
    echo "Terjadi kesalahan ketika upload <br>";
    echo "<a href='index.php'>Kembali</a>";
    die();
}
$mobil->gambar = $namaFile;
#4. Panggil fungsi fungsi insert
$mobil->insert();

#5. Redirect ke halaman index dari mobil
header('Location: /../../index.php?page=list-mobil');